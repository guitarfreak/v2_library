<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermitsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $queryString = /** @lang text */
            "
                INSERT INTO `permits` (
                    `user_id`, 
                    `expiry_date`
                )
                VALUES
                    (1, '{$this->generateExpiryDate()}'),
                    (2, '{$this->generateExpiryDate()}'),
                    (3, '{$this->generateExpiryDate()}'),
                    (4, '{$this->generateExpiryDate()}'),
                    (5, '{$this->generateExpiryDate()}'),
                    (6, '{$this->generateExpiryDate()}'),
                    (7, '{$this->generateExpiryDate()}'),
                    (8, '{$this->generateExpiryDate()}'),
                    (9, '{$this->generateExpiryDate()}'),
                    (10, '{$this->generateExpiryDate()}')
            ";

        DB::statement($queryString);
    }

    /**
     * Generates emission date
     *
     * @return string
     */
    public function generateEmissionDate(): string
    {
        return date('Y-m-d');
    }

    /**
     * Generates expiry date
     *
     * @return string
     */
    public function generateExpiryDate(): string
    {
        return date('Y-m-d', strtotime('+2 weeks'));
    }
}

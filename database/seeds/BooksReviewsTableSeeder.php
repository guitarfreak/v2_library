<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BooksReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $queryString = /** @lang text */
            " 
                UPDATE `books` 
                SET reviews = 'Laravel includes a simple method of seeding your database with test data using seed classes.'
                WHERE id IS NOT NULL;
           ";

        DB::statement($queryString);
    }
}

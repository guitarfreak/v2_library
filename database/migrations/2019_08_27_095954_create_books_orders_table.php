<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateBooksOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $queryString = /** @lang text */
            '
                CREATE TABLE `books_orders` (
                  `order_id` INT(11) UNSIGNED NOT NULL,
                  `book_id` INT(11) UNSIGNED NOT NULL,
                PRIMARY KEY (`order_id`, `book_id`),
                  
                CONSTRAINT `fk_orders_user_orders` FOREIGN KEY (`order_id`) REFERENCES `user_orders` (`order_id`) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
                
                CONSTRAINT `fk_orders_books` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) 
                ON UPDATE CASCADE ON DELETE CASCADE
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            ';


        DB::statement($queryString);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $queryString = /** @lang text */
            '
                DROP TABLE `books_orders`;
            ';

        DB::statement($queryString);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class PermitsSetUuidDefaultValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->dropIdColumn();
        $this->dropcodeUUIDColumn();
        $this->createUUIDcolumn();
        $this->createUUIDtrigger();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->dropUUIDColumn();
        $this->createIdColumn();
    }

    /**
     * Drops `id` column
     */
    protected function dropIdColumn(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `permits`
                DROP COLUMN `id`;
            ';

        DB::statement($queryString);
    }

    /**
     * Drops `id` column
     */
    protected function dropcodeUUIDColumn(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `permits`
                DROP COLUMN `code_uuid`;
            ';

        DB::statement($queryString);
    }


    /**
     * Makes `uuid_code` primary key
     */
    protected function createUUIDcolumn(): void
    {
        $queryString = /** @lang text */
            '
              ALTER TABLE `permits`
              ADD COLUMN `uuid` CHAR(36) NULL FIRST;
            ';

        DB::statement($queryString);
    }

    /**
     * Triggers auto fill for default
     */
    protected function createUUIDtrigger(): void
    {
        $queryString = /** @lang text */
            '
                CREATE TRIGGER before_insert_permits
                BEFORE INSERT ON permits
                FOR EACH ROW
                BEGIN
                  IF new.uuid IS NULL THEN
                    SET new.uuid = uuid();
                  END IF;
                END
            ';

        DB::connection()->getPdo()->exec($queryString);
    }

    /**
     * Drops `id` column
     */
    protected function dropUUIDColumn(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `permits`
                DROP COLUMN `uuid`;
            ';

        DB::statement($queryString);
    }

    /**
     * Drops uuid column
     */
    protected function createIdColumn(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `permits`
                ADD COLUMN `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;
            ';

        DB::statement($queryString);
    }
}

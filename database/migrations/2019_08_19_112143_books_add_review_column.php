<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class BooksAddReviewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `books`
                ADD COLUMN `reviews` LONGTEXT NULL;
            ';

        DB::statement($queryString);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `books` 
                DROP COLUMN `reviews`;
            ';

        DB::statement($queryString);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UsersTblChangeColName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `users` CHANGE COLUMN `year_of_birth` `date_of_birth` DATE NOT NULL;
            ';

        DB::statement($queryString);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `users` CHANGE COLUMN `date_of_birth` `year_of_birth` DATE NOT NULL;
            ';

        DB::statement($queryString);
    }
}

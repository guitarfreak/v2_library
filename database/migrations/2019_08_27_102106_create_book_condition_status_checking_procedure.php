<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateBookConditionStatusCheckingProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $queryString = /** @lang text */
            "
                CREATE PROCEDURE updateBooksStatus()
                BEGIN
                    SELECT b.condition, b.date_added FROM `books` AS b;
                    
                    IF DATEDIFF('2019-08-27', date) >= 365
                        SELECT condition
                        CASE
                            WHEN condition = 'mint' THEN SET 'new'
                            WHEN condition = 'new' THEN SET 'medium'
                            WHEN condition = 'medium' THEN SET 'poor'
                        END
                        FROM `books`;
                    END IF; 
                END
            ";

//        DB::connection()->getPdo()->exec($queryString);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $queryString = /** @lang text */
            '
                DROP PROCEDURE IF EXISTS updateBooksStatus;;
            ';

//        DB::statement($queryString);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeUserOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->renameIdKey();
        $this->dropFK();
        $this->dropBookIdColumn();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->restoreIdColumn();
        $this->createBookIdColumn();
    }

    /**
     * Renames key `id` to `order_id`
     */
    protected function renameIdKey(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `user_orders` 
                CHANGE COLUMN `id` `order_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;
            ';

        DB::statement($queryString);
    }

    protected function dropFK(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `user_orders` 
                DROP FOREIGN KEY `fk9_orders_users`;
            ';

        DB::statement($queryString);
    }

    /**
     * Drops column `book_id`
     */
    protected function dropBookIdColumn(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `user_orders` 
                DROP `book_id`;
            ';

        DB::statement($queryString);
    }

    protected function restoreIdColumn(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `user_orders` 
                CHANGE COLUMN `order_id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;
            ';

        DB::statement($queryString);
    }

    protected function createBookIdColumn(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `user_orders` 
                ADD COLUMN `book_id` INT(11) UNSIGNED NULL;
            ';

        DB::statement($queryString);
    }

    protected function addFKBookId(): void
    {
        $queryString = /** @lang text */
            '
                ALTER TABLE `user_orders` 
                ADD FOREIGN KEY fk9_orders_users(`book_id`)
                REFERENCES `books` (`id`) 
                ON UPDATE CASCADE ON DELETE SET NULL;
            ';

        DB::statement($queryString);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $queryString = /** @lang text */
            '
                CREATE TABLE `user_orders` (
                    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    `user_id` INT(11) UNSIGNED NULL,
                    `book_id` INT(11) UNSIGNED NULL,
                    `start_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
                    `due_date` DATE NULL,
                    
                   CONSTRAINT `fk9_orders_users` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) 
                   ON UPDATE CASCADE ON DELETE SET NULL, 
                
                   CONSTRAINT `fk10_orders_books` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
                   ON UPDATE CASCADE ON DELETE SET NULL 
                   
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            ';

        DB::statement($queryString);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $queryString = /** @lang text */
            '
                DROP TABLE `user_orders`;
            ';

        DB::statement($queryString);
    }
}
